# NodeJS-MongoDB-Demo

This is an example NodeJS application that works with MongoDB on Docker/K8s

The application is a very simple list where you can add or delete values.

To install it, simply fork this repository and create an application from your GitHub/Gitlab repo.
Then create a MongoDB add-on and link it to your application.

CI steps:
1. Build the image
2. Push the image to DockerHub

That's it, the application will use the environnement variables to connect to MongoDB.

Set these (environment) Variables as well:

+ CI_REGISTRY
+ CI_REGISTRY_IMAGE
+ CI_REGISTRY_PASSWORD
+ CI_REGISTRY_USER
+ CI_TAG
